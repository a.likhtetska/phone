public abstract class Phone implements PhoneConnection, PhoneMedia {

 String name;
 String model;
int storage;
int RAM;
String color;
  int price;


 public Phone(String name, String model, int storage, int RAM, String color, int price) {
    this.name = name;
   this.model = model;
       this.storage = storage;
      this.RAM = RAM;
      this.color = color;
       this.price = price; }

    @Override
    public void call() {

    }

    @Override
    public void message() {

    }

    @Override
    public void photo() {

    }

    @Override
    public void video() {

    }

    @Override
    public String toString() {
        return "Phone{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", storage=" + storage +
                ", RAM=" + RAM +
                ", color='" + color + '\'' +
                ", price=" + price +
                '}';
    }
}


//public abstract String Name;
        //public abstract String Model;
        // public abstract int getStorage();
        //public abstract int getRAM();
        //public abstract String getColor();
        // public abstract int getPrice();

        // @Override
        // public String toString() {
        //    return "Phone{}";
