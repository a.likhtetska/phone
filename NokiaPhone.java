public class NokiaPhone extends Phone {
    public NokiaPhone(String name, String model, int storage, int RAM, String color, int price) {
        super(name, model, storage, RAM, color, price);
    }

    @Override
    public void call() {
        super.call();
    }

    @Override
    public void message() {
        super.message();
    }
}
    //public abstract class NokiaPhone implements PhoneConnection {

