public class MiPhone extends Phone {
    public MiPhone(String name, String model, int storage, int RAM, String color, int price) {
        super(name, model, storage, RAM, color, price);
    }

    @Override
    public void photo() {
        super.photo();
    }

    @Override
    public void video() {
        super.video();
    }

    @Override
    public void call() {
        super.call();
    }

    @Override
    public void message() {
        super.message();
    }
}

    //implements PhoneConnection, PhoneMedia{

