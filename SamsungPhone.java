public class SamsungPhone extends Phone {
    public SamsungPhone(String name, String model, int storage, int RAM, String color, int price) {
        super(name, model, storage, RAM, color, price);
    }

    @Override
    public void call() {
        super.call();
    }

    @Override
    public void photo() {
        super.photo();
    }

    @Override
    public void message() {
        super.message();
    }

    @Override
    public void video() {
        super.video();
    }
}

    //implements PhoneConnection, PhoneMedia {
